Name:           perl-Template-Multilingual
Version:        1.00
Release:        1
Summary:        Multilingual templates for Template Toolkit
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Template-Multilingual
Source0:        https://cpan.metacpan.org/authors/id/C/CH/CHOLET/Template-Multilingual-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  perl-interpreter
BuildRequires:  perl-generators
BuildRequires:  perl(Module::Build)
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
# runtime deps
BuildRequires:  perl(Template)
BuildRequires:  perl(Template::Parser)
BuildRequires:  perl(base)
# test deps
BuildRequires:  perl(Test::More)

%{?perl_default_filter}

%description
This subclass of Template Toolkit's Template class supports multilingual
templates: templates that contain text in several languages.

%prep
%setup -q -n Template-Multilingual-%{version}

%build
%{__perl} Build.PL --installdirs=vendor
./Build

%install
./Build install --destdir=$RPM_BUILD_ROOT --create_packlist=0
%{_fixperms} $RPM_BUILD_ROOT/*

%check
./Build test

%files
%doc Changes README
%{perl_vendorlib}/Template*
%{_mandir}/man3/Template*

%changelog
* Mon Apr 22 2024 xuhe <xuhe@kylinos.cn> - 1.00-1
- Package init
